let latestMovieData = "";
const moviesContainer = document.getElementById('moviesContainer');
const libraryMoviesContainer = document.getElementById('libraryMoviesContainer');

let seacrhMoviesUrl = new URL(process.env.API_SEARCH_MOVIE);
seacrhMoviesUrl.searchParams.set('api_key', process.env.API_KEY);

let myLibraryData = [];

export const callMoviesApi = (url) => {
    console.log('callMoviesApi api called')
    fetch(url)
    .then((response) => response.json())
    .then((movies) => {
        console.log('movies',movies)
        latestMovieData = movies.results;
        sortMovies()
    })
    .catch((error) => {
        console.error('error',error)
    })
}

export const dumpMovies = (movies) => {
    moviesContainer.innerHTML = "";
    for (var movie of movies) {
        let template = '<div class="movie-item">';
        if(movie.poster_path)
            template += `<img class="poster" src="${process.env.API_IMG_BASE}${movie.poster_path}" />`;
        if(movie.id)
            template += `<div class="add-movie" data-movieId="${movie.id}">Add</div>`;
        template += '<div class="details"">';
            if(movie.title)
                template += `<span class="title">${movie.title}</span>`;
            if(movie.release_date)
                template += `<span class="release_date">${getYear(movie.release_date)}</span>`;
        template += '</div>';
        template += '</div>';
        moviesContainer.innerHTML += template;
    }
}

export const sortMovies = (movies = latestMovieData, sortByDefault = "title" ) => {
    let sortBy = document.getElementById("filter").value || sortByDefault;
    if(sortBy != "") {
        console.log('sortMovies with ', sortBy)
        let sorter = movies[0][sortBy];
        if(isNaN(sorter)) {
            movies.sort((a, b) => {
                var nameA = a[sortBy].toUpperCase();
                var nameB = b[sortBy].toUpperCase();
                if (nameA < nameB) {
                    return -1;
                }
                if (nameA > nameB) {
                    return 1;
                }
                return 0;
            });
        }
        else {
            movies.sort((a, b) => a[sortBy] - b[sortBy]);
        }
    }
    dumpMovies(movies)
}

export const getYear = (date) => new Date(date).getFullYear();

export const searchMovie = () => {
    console.log('seacrhMoviesUrl api called')
    let searchParam = document.getElementById("searchText").value.trim();
    if (searchParam.length === 0)
    { 
       alert("Enter something to search");  	
       return false; 
    }
    seacrhMoviesUrl.searchParams.set('query', searchParam);
    callMoviesApi(seacrhMoviesUrl);
}

const filterItems = (arr, query) => {
    return arr.filter(el => el.id == query);
};

const removefilterItems = (arr, query) => {
    return arr.filter(el => el.id != query);
};

export const addToLibrary = (id) => {
    const myLibraryArr = filterItems(myLibraryData, id);
    if( myLibraryArr.length === 0 ) {
        const filterArr = filterItems(latestMovieData, id);
        myLibraryData = [ ...myLibraryData, ...filterArr];
        console.log('myLibraryData', myLibraryData);
        dumpLibraryMovies(myLibraryData);
        alert('Added to library successfully');
    }
    else {
        alert('Already added to library');
    }
}

export const removeFromLibrary = (id) => {
    myLibraryData = removefilterItems(myLibraryData, id);
    console.log('myLibraryData',myLibraryData)
    dumpLibraryMovies(myLibraryData);
    alert('Removed from library successfully');
}

const dumpLibraryMovies = (movies) => {
    libraryMoviesContainer.innerHTML = "";
    for (var movie of movies) {
        let template = '<div class="movie-item">';
        if(movie.poster_path)
            template += `<img class="poster" src="${process.env.API_IMG_BASE}${movie.poster_path}" />`;
        if(movie.id)
            template += `<div class="remove-movie" data-movieId="${movie.id}">Remove</div>`;
        template += '<div class="details"">';
            if(movie.title)
                template += `<span class="title">${movie.title}</span>`;
            if(movie.release_date)
                template += `<span class="release_date">${getYear(movie.release_date)}</span>`;
        template += '</div>';
        template += '</div>';
        libraryMoviesContainer.innerHTML += template;
    }
}