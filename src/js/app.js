import { callMoviesApi, sortMovies, searchMovie, addToLibrary, removeFromLibrary } from './main';
require('dotenv').config()

let upcomingMoviesUrl = new URL(process.env.API_UPCOMING_MOVIE);
upcomingMoviesUrl.searchParams.set('api_key', process.env.API_KEY);

// Initial Api Call
callMoviesApi(upcomingMoviesUrl);

// Call search on click seacrh btn
document.getElementById('searchBtn').addEventListener('click', () => searchMovie() );

// sort the movie on filter change option
document.getElementById("filter").addEventListener('change', () => sortMovies() );

document.body.addEventListener('click', function (evt) {
    if (evt.target.classList.contains('add-movie')) {
	    addToLibrary(evt.target.dataset.movieid)
    }
    else if (evt.target.classList.contains('remove-movie')) {
	    removeFromLibrary(evt.target.dataset.movieid)
    }
    else if (evt.target.classList.contains('change-view') && !evt.target.classList.contains('active')) {

        let activeElems = document.getElementsByClassName("change-view");
        [...activeElems].forEach(element => {
            element.classList.toggle("active");
        });

        let elems = document.getElementsByClassName("toggle-view");
        [...elems].forEach(element => {
            element.classList.toggle("hide");
        });
    }
}, false);



